package com.example.cs.bxdriver;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 10-08-2016.
 */
public class LoginActivity extends AppCompatActivity {
    private EditText mPhone, mPassword;
    Button loginBtn;
    TextView forgotPwd;
    String response;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        mPhone = (EditText) findViewById(R.id.login_phone);
        mPassword = (EditText) findViewById(R.id.login_pwd);
        forgotPwd = (TextView) findViewById(R.id.forgot_pwd);
        loginBtn = (Button) findViewById(R.id.login_btn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mPhone.getText().toString();
                String password = mPassword.getText().toString();
                if (email.length() == 0) {
//                    if(language.equalsIgnoreCase("En")) {
                        mPhone.setError("Please enter Mobile number");
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        mEmail.setError("من فضلك أدخل رقم الجوال");
//                    }
                    mPhone.requestFocus();
                }else if(mPhone.length() != 9){
//                    if(language.equalsIgnoreCase("En")) {
                        mPhone.setError("Please enter valid Mobile Number");
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        mEmail.setError("من فضلك ادخل رقم جوال صحيح");
//                    }

                }else if (password.length() == 0) {
//                    if(language.equalsIgnoreCase("En")) {
                        mPassword.setError("Please enter Password");
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        mPassword.setError("من فضلك ادخل كلمة السر");
//                    }

                    mPassword.requestFocus();
                } else {
                    new CheckLoginDetails().execute(Constants.LOGIN_URL+"966"+email+"?psw="+password+"&dtoken="+SplashActivity.regid+"&lan=En");
                }
            }
        });



        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
//                startActivity(intent);
            }
        });
    }



    public class CheckLoginDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
            dialog = ProgressDialog.show(LoginActivity.this, "",
                    "Checking login...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoginActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isDriver = jo1.getBoolean("IsDriver");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");

                                if(isDriver) {
                                    try {
                                        JSONObject parent = new JSONObject();
                                        JSONObject jsonObject = new JSONObject();
                                        JSONArray jsonArray = new JSONArray();
                                        jsonArray.put("lv1");
                                        jsonArray.put("lv2");

                                        jsonObject.put("userId", userId);
                                        jsonObject.put("fullName", fullName);
                                        jsonObject.put("mobile", mobile);
                                        jsonObject.put("email", email);
                                        jsonObject.put("language", language);
                                        jsonObject.put("family_name", familyName);
                                        jsonObject.put("nick_name", nickName);
                                        jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                        jsonObject.put("user_details", jsonArray);
                                        parent.put("profile", jsonObject);
                                        Log.d("output", parent.toString());
                                        userPrefEditor.putString("user_profile", parent.toString());
                                        userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                        userPrefEditor.commit();

//                                    if(isVerified) {
                                        userPrefEditor.putString("login_status", "loggedin");
                                        userPrefEditor.commit();
//                                        Intent loginIntent = new Intent();
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog));

//                if (language.equalsIgnoreCase("En")) {
                                        // set title
                                        alertDialogBuilder.setTitle("Broasted Express");

                                        // set dialog message
                                        alertDialogBuilder
                                                .setMessage("Are you \" "+fullName+" "+familyName+"\"")
                                                .setCancelable(false)
                                                .setPositiveButton("confirm", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                                        startActivity(i);
//                                                                Log.e("TAG",""+userId);
                                                        finish();
                                                        dialog.dismiss();
                                                    }
                                                });
                                        alertDialogBuilder
                                                .setNegativeButton("edit", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        Intent a=new Intent(LoginActivity.this,EditProfile.class);
                                                        startActivity(a);
                                                    }
                                                });
//                } else if (language.equalsIgnoreCase("Ar")) {
                                        // set title
//                                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                                        // set dialog message
//                                        alertDialogBuilder
//                                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات"+myDbHelper.getTotalOrderQty()+ "لديك " )
//                                                .setCancelable(false)
//                                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
//                                                    public void onClick(DialogInterface dialog, int id) {
//                                                        myDbHelper.deleteOrderTable();
//                                                        Checkout.morder_quantity.setText("0");
//                                                        Checkout.morder_price.setText("0ريال");
//                                                        Checkout.mcount_basket.setText("0");
//                                                        Menu.morder_quantity.setText("0");
//                                                        Menu.morder_price.setText("0ريال");
//                                                        Menu.mcount_basket.setText("0");
//                                                        InsideMenu.morder_quantity.setText("0");
//                                                        InsideMenu.morder_price.setText("0ريال");
//                                                        InsideMenu.mcount_basket.setText("0");
//                                                        Intent a=new Intent(Checkout.this,Menu.class);
//                                                        startActivity(a);
//                                                    }
//                                                });
//                                        alertDialogBuilder
//                                                .setNegativeButton("لا", new DialogInterface.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(DialogInterface dialog, int which) {
//                                                        dialog.dismiss();
//                                                    }
//                                                });
//                }

//
                                        // create alert dialog
                                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                                        alertDialog.show();
//                                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
//                                        startActivity(i);
//                                        finish();
//                                    }else{
//                                        Intent loginIntent = new Intent(LoginActivity.this, VerifyRandomNumber.class);
//                                        loginIntent.putExtra("phone_number", mobile);
//                                        startActivityForResult(loginIntent, VERIFICATION_REQUEST);
//                                        finish();
//                                    }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }else{
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog));

                                    // set title
                                    alertDialogBuilder.setTitle("Broasted Express");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("You are not assigned as a driver! Please contact operations team.")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });

                                    // create alert dialog
                                    AlertDialog alertDialog = alertDialogBuilder.create();

                                    // show it
                                    alertDialog.show();
                                }
                            }catch (JSONException je){
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog));

//                                if(language.equalsIgnoreCase("En")) {
//                                    // set title
//                                    alertDialogBuilder.setTitle("Oregano");
//
//                                    // set dialog message
//                                    alertDialogBuilder
//                                            .setMessage("Invalid User")
//                                            .setCancelable(false)
//                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                }else if(language.equalsIgnoreCase("Ar")){
                                    // set title
                                    alertDialogBuilder.setTitle("Broasted Express");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Invalid User")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
//                                }


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
}
