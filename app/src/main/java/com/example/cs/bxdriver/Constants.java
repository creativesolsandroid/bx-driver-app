package com.example.cs.bxdriver;

/**
 * Created by CS on 09-06-2016.
 */
public class Constants {

//    static String live_url = "http://csadms.com/BXServices";
    static String live_url = "http://www.alrajhiksa.com/";
//    static String live_url = "http://85.194.94.241/OreganoServices";

    public static String CATEGORY_ITEMS_URL = live_url+"/api/SubCategoryApi/";
    public static String ADDITIONALS_URL = live_url+"/Api/ModifierInfoList/";
    public static String STORES_URL = live_url+"/api/StoreInformationApi/";
    public static String LOGIN_URL = live_url+"/api/RegistrationApi/Signin/";
    public static String REGISTRATION_URL = live_url+"/api/RegistrationApi";
    public static String INSERT_ORDER_URL = live_url+"/api/OrderDetailsApi";
    public static String ORDER_HISTORY_URL = live_url+"/api/OrderTrackApi/";
    public static String SAVE_ADDRESS_URL = live_url+"/api/RegistrationApi/";
    public static String TRACK_ORDER_STEPS_URL = live_url+"/api/OrderTrackApi?orderId=";
    public static String SAVED_ADDRESS_URL = live_url+"/api/RegistrationApi/";
    public static String GET_FAVORITE_ORDERS_URL = live_url+"/api/FaviorateOrderApi/";
    public static String DELETE_FAVORITE_ORDERS_URL = live_url+"/api/FaviorateOrderApi/";
    public static String INSERT_FAVORITE_ORDER_URL = live_url+"/api/FaviorateOrderApi?OrderId=";
    public static String CHANGE_PASSWORD_URL = live_url+"/api/VerifyUserCredentialsApi/";
    public static String ORDERD_DETAILS_URL = live_url+"/api/OrderDetailsApi?OrderId=";
    public static String VERIFY_RANDOM_NUMBER_URL = live_url+"/api/RegistrationApi?MobileNo=";
    public static String GET_SAVED_CARDS_URL = live_url+"/api/CreditCardDetails?userId=";
    public static String SAVE_CARD_DETAILS_URL = live_url+"/api/CreditCardDetails?userId=";
    public static String FORGOT_PASSWORD_URL = live_url+"/api/VerifyUserCredentialsApi?UsrName=";
    public static String GET_CURRENT_TIME_URL = live_url+"/api/RegistrationApi/GetCurrentTime";
    public static String EDIT_ADDRESS_URL = live_url+"/api/RegistrationApi";
    public static String EDIT_USER_PROFILE_URL = live_url + "/api/RegistrationApi/UpdateUserProfile";
    public static String GET_MESSAGES_URL = live_url+"/api/PushMsg?userid=";
    public static String GET_OFFERS_URL = live_url+"/api/OfferDetails?offerId=";
    public static String UPDATE_MESSAGE_URL = live_url+"/api/PushMsg";
    public static String UPDATE_PROFILE_URL = live_url+"/api/VerifyUserCredentialsApi";
    public static String GET_BANNERS_URL = live_url+"/api/OfferDetails";
    public static String GET_PROMOS_URL = live_url+"/api/OreganoPromotions?userId=";
    public static String LIVE_TRACKING_URL = live_url+"/api/OrderTrackApi?OrderId=";
    public static String DELETE_ORDER_FROM_HISTORY = live_url + "OrderDetailsApi?Delete_OrderID=";


//    static String local_url = "http://192.168.1.102";

//    public static String CATEGORY_ITEMS_URL = local_url+"/OreganoLive/api/SubCategoryApi/";
//    public static String ADDITIONALS_URL = local_url+"/OreganoLive/Api/ModifierInfoList/";
//    public static String STORES_URL = local_url+"/OreganoLive/api/StoreInformationApi/saturday";
//    public static String LOGIN_URL = local_url+"/OreganoLive/api/VerifyUserCredentialsApi/";
//    public static String REGISTRATION_URL = local_url+"/OreganoLive/api/RegistrationApi";
//    public static String INSERT_ORDER_URL = local_url+"/OreganoLive/api/OrderDetailsApi";
//    public static String ORDER_HISTORY_URL = local_url+"/OreganoLive/api/OrderDetailsApi/";
//    public static String SAVE_ADDRESS_URL = local_url+"/OreganoLive/api/RegistrationApi/";
//    public static String TRACK_ORDER_STEPS_URL = local_url+"/OreganoLive/api/OrderTrackApi?orderId=";
//    public static String SAVED_ADDRESS_URL = local_url+"/OreganoLive/api/RegistrationApi/";
//    public static String GET_FAVORITE_ORDERS_URL = local_url+"/OreganoLive/Api/FaviorateOrderApi/";
//    public static String DELETE_FAVORITE_ORDERS_URL = local_url+"/OreganoLive/Api/FaviorateOrderApi/";
//    public static String INSERT_FAVORITE_ORDER_URL = local_url+"/OreganoLive/Api/FaviorateOrderApi?OrderId=";
//    public static String CHANGE_PASSWORD_URL = local_url+"/OreganoLive/api/VerifyUserCredentialsApi/";
//    public static String ORDERD_DETAILS_URL = local_url+"/OreganoLive/api/OrderDetailsApi?OrderId=";
//    public static String VERIFY_RANDOM_NUMBER_URL = local_url+"/OreganoLive/api/RegistrationApi?MobileNo=";
//    public static String GET_SAVED_CARDS_URL = local_url+"/OreganoLive/api/CreditCardDetails?userId=";
//    public static String SAVE_CARD_DETAILS_URL = local_url+"/OreganoLive/api/CreditCardDetails?userId=";
//    public static String FORGOT_PASSWORD_URL = local_url+"/OreganoLive/api/VerifyUserCredentialsApi?UsrName=";
//    public static String GET_CURRENT_TIME_URL = "http://192.168.1.102/OreganoLive/api/GetCurrentTime";

    public static String ORDER_TYPE = "";
    public static String COMMENTS = "";
}
