package com.example.cs.bxdriver;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CS on 19-08-2016.
 */
public class PendingFragment extends Fragment {
    ListView ordersListview;
    SharedPreferences userPrefs;
    String userId;
    private ArrayList<Order> ordersList = new ArrayList<>();
    OrdersAdapter mAdapter;
    int pos;
    AlertDialog alertDialog;

    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    private Timer timer = new Timer();
    private SwipeRefreshLayout swipeLayout;
    boolean loading = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.new_fragment, container,
                false);
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        ordersListview = (ListView) rootView.findViewById(R.id.orders_listview);
        swipeLayout = (SwipeRefreshLayout) rootView
                .findViewById(R.id.swipe_container);
        mAdapter = new OrdersAdapter(getActivity(), ordersList);

        ordersListview.setAdapter(mAdapter);

        new GetOrderDetails().execute(Constants.live_url+"/api/DriverApp/GetAssignOrders?DriverId="+userId);

        ordersListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if(ordersList.get(position).getOrderStatus().equalsIgnoreCase("Accepted") || ordersList.get(position).getOrderStatus().equalsIgnoreCase("Ready")){
                    Intent i = new Intent(getActivity(), StoreDetails.class);
                    i.putExtra("order_obj", ordersList.get(pos));
                    startActivity(i);
                }else{
                    Intent i = new Intent(getActivity(), CustomerDetails.class);
                    i.putExtra("order_obj", ordersList.get(pos));
                    startActivity(i);
                }

            }
        });
        timer.schedule(new MyTimerTask(), 60000, 120000);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                if(!loading) {
                    loading = true;
                    new GetOrderDetails().execute(Constants.live_url+"/api/DriverApp/GetAssignOrders?DriverId="+userId);
                }else{
                    swipeLayout.setRefreshing(false);
                }

            }
        });
        swipeLayout.setColorScheme(new int[]{android.R.color.holo_blue_bright, android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light});

        return rootView;
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new GetOrderDetails().execute(Constants.live_url+"/api/DriverApp/GetAssignOrders?DriverId="+userId);
                    }
                });
            }
        }
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ordersList.get(pos).getStoreMobile()));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getActivity(), "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            ordersList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    Order oh = new Order();
                                    JSONObject jo1 = ja.getJSONObject(i);



                                    oh.setOrderId(jo1.getString("orderId"));
                                    oh.setStoreId(jo1.getString("storeId"));
                                    oh.setStoreName(jo1.getString("StoreName"));
                                    oh.setStoreAddress(jo1.getString("StoreAddress"));
                                    oh.setUserId(jo1.getString("UserId"));

                                    oh.setUserMobile(jo1.getString("usrMobile"));
                                    oh.setStoreMobile(jo1.getString("StrPhone"));
                                    oh.setUserLat(jo1.getString("usrLatitude"));
                                    oh.setUserLong(jo1.getString("usrLongitude"));
                                    oh.setStoreLat(jo1.getString("strLatitude"));
                                    oh.setStoreLong(jo1.getString("strLongitude"));
                                    oh.setComments(jo1.getString("comments"));
                                    oh.setOrderDate(jo1.getString("orderDate"));
                                    oh.setExpectedTime(jo1.getString("ExpectedTime"));

                                    oh.setFullName(jo1.getString("FullName"));
                                    oh.setAddress(jo1.getString("Address"));
                                    oh.setHouseNo(jo1.getString("houseno"));
                                    oh.setLandmark(jo1.getString("landmark"));
                                    oh.setTotalPrice(jo1.getString("Total_Price"));
                                    oh.setPaymentMode(jo1.getString("PaymentMode"));
                                    oh.setInvoiceNo(jo1.getString("InvoiceNo"));
                                    oh.setOrderStatus(jo1.getString("OrderStatus"));

                                    if(!jo1.getString("OrderStatus").equalsIgnoreCase("Pending")){
                                        ordersList.add(oh);
                                    }


                                }
                            }catch (JSONException je){
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            loading = false;
            swipeLayout.setRefreshing(false);
            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }



    @Override
    public void onDetach() {
        super.onDetach();
        timer.cancel();
    }
}

