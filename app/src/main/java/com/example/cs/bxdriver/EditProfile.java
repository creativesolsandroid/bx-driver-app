package com.example.cs.bxdriver;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class EditProfile extends AppCompatActivity {
    EditText mFirstName, mFamilyName, mNickName, mPhoneNumber, mEmail;
    TextView mGenderMale, mGenderFemale, mGenderMale1, mGenderFemale1;
    Button mUpdate;
    private String firstName, familyName, nickName, phoneNumber, email, gender = "Male";
    String response, language="En", userId;
    SharedPreferences languagePrefs;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
//        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.edit_profile);
//        } else if (language.equalsIgnoreCase("Ar")) {
//            setContentView(R.layout.content_edit_profile_arabic);
//        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        String profile = userPrefs.getString("user_profile", null);

        mFirstName = (EditText) findViewById(R.id.name);
        mFamilyName = (EditText) findViewById(R.id.family_name);
//        mNickName = (EditText) findViewById(R.id.nick_name);
//        mPhoneNumber = (EditText) findViewById(R.id.mobile_number);
//        mEmail = (EditText) findViewById(R.id.email);

//        mGenderMale = (TextView) findViewById(R.id.gender_male);
//        mGenderFemale = (TextView) findViewById(R.id.gender_female);
//        mGenderMale1 = (TextView) findViewById(R.id.gender_male1);
//        mGenderFemale1 = (TextView) findViewById(R.id.gender_female1);

        mUpdate = (Button) findViewById(R.id.update);

//        mGenderMale.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gender = "Male";
//                mGenderMale1.setVisibility(View.VISIBLE);
//                mGenderFemale1.setVisibility(View.GONE);
//            }
//        });
//        mGenderFemale.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gender = "Female";
//                mGenderFemale1.setVisibility(View.VISIBLE);
//                mGenderMale1.setVisibility(View.GONE);
//            }
//        });

        if (profile != null) {
            try {

                JSONObject property = new JSONObject(profile);
                JSONObject userobject = property.getJSONObject("profile");

//                String parts[] = userObjuect.getString("phone").split("-");

//                userId = userObjuect.getString("userId");
                mFirstName.setText(userobject.getString("fullName"));
                mFamilyName.setText(userobject.getString("family_name"));
//                mNickName.setText(userobject.getString("nick_name"));
//                mPhoneNumber.setText("+" + userobject.getString("mobile"));
//                mEmail.setText(userobject.getString("email"));
//                gender = userobject.getString("gender");

            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }

        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject parent = new JSONObject();

                firstName = mFirstName.getText().toString();
                familyName = mFamilyName.getText().toString();
//                nickName = mNickName.getText().toString();
//                phoneNumber = mPhoneNumber.getText().toString();
//                email = mEmail.getText().toString().replaceAll("", "");

                if (firstName.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                        mFirstName.setError("Please enter First Name");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mFirstName.setError("من فضلك ارسل الاسم بالكامل");
//                    }
                } else if(familyName.length() == 0){
//                    if(language.equalsIgnoreCase("En")) {
                    mFamilyName.setError("Please enter Last Name");
//                    }else if(language.equalsIgnoreCase("Ar")){
//                        mFirstName.setError("من فضلك ارسل الاسم بالكامل");
//                    }
                }
//                } else if (phoneNumber.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
//                        mPhoneNumber.setError("Please enter Mobile Number");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mPhoneNumber.setError("من فضلك أدخل رقم الجوال");
//                    }
//                } else if (email.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
//                        mEmail.setError("Please enter Email");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mEmail.setError("من فضلك ادخل البريد الالكتروني");
//                    }
//                } else if (!email.matches("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}")) {
//                    if (language.equalsIgnoreCase("En")) {
//                        mEmail.setError("Please use Email format (example - abc@abc.com");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mEmail.setError("من فضلك استخدم صيغة البريد الالكتروني (example - abc@abc.com)");
//                    }
                 else {
                    try {
                        JSONArray mainItem = new JSONArray();


                        JSONObject mainObj = new JSONObject();
                        mainObj.put("FullName", firstName);
                        mainObj.put("FamilyName", familyName);
                        mainObj.put("NickName", "");
                        mainObj.put("Gender", gender);
                        mainObj.put("Email", "");
                        mainObj.put("DeviceToken", "");
                        mainObj.put("Language", language);
                        mainObj.put("UserId", userId);
                        mainItem.put(mainObj);

                        parent.put("UserProfile", mainItem);
                        Log.i("Tag", parent.toString());

                    } catch (JSONException e) {
                    }
                    new InsertRegistration().execute(parent.toString());
                }
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                View view = this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        super.onBackPressed();
    }

    public class InsertRegistration extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EditProfile.this);
            dialog = ProgressDialog.show(EditProfile.this, "",
                    "Updating...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        HttpClient httpclient = new DefaultHttpClient();

                        HttpPut httpput = new HttpPut(Constants.EDIT_USER_PROFILE_URL);

                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        httpput.setEntity(se);

                        httpput.setHeader("Accept", "application/json");
                        httpput.setHeader("Content-type", "application/json");

                        HttpResponse httpResponse = httpclient.execute(httpput);

                        inputStream = httpResponse.getEntity().getContent();

                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.e("TAG", "" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {

                return "no internet";
            }
        }


        @Override
        public void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(EditProfile.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(EditProfile.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");


                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    Toast.makeText(EditProfile.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                                    finish();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException je) {
                                je.printStackTrace();
                                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(new ContextThemeWrapper(EditProfile.this, android.R.style.Theme_Material_Light_Dialog));

                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("Broasted Express");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(result)
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle(" بروستد إكسبريس");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(result)
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }


                                // create alert dialog
                                android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(EditProfile.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

}

