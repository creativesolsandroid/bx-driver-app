package com.example.cs.bxdriver;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by CS on 18-08-2016.
 */
public class HistoryFragment extends Fragment {

    ListView ordersListview;
    SharedPreferences userPrefs;
    String userId;
    private ArrayList<OrderHistory> ordersList = new ArrayList<>();
    OrderHistoryAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.history_fragment, container,
                false);
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        ordersListview = (ListView) rootView.findViewById(R.id.orders_history_listview);

        mAdapter = new OrderHistoryAdapter(getActivity(), ordersList);

        ordersListview.setAdapter(mAdapter);

        new GetOrderDetails().execute(Constants.live_url+"/api/DriverApp/GetDriverOrderHistory?DriverIdForHistory="+userId);
        return rootView;
    }

    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            ordersList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    OrderHistory oh = new OrderHistory();
                                    JSONObject jo1 = ja.getJSONObject(i);



                                    oh.setOrderId(jo1.getString("orderId"));
                                    oh.setStoreId(jo1.getString("storeId"));
                                    oh.setStoreName(jo1.getString("StoreName"));
                                    oh.setUserId(jo1.getString("UserId"));

                                    oh.setComments(jo1.getString("comments"));
                                    oh.setOrderDate(jo1.getString("orderDate"));
                                    oh.setExpectedTime(jo1.getString("ExpectedTime"));

                                    oh.setTotalPrice(jo1.getString("Total_Price"));
                                    oh.setPaymentMode(jo1.getString("PaymentMode"));
                                    oh.setInvoiceNo(jo1.getString("InvoiceNo"));
                                    oh.setOrderStatus(jo1.getString("OrderStatus"));

                                    ordersList.add(oh);

                                }
                            }catch (JSONException je){
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(getActivity(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }
}
