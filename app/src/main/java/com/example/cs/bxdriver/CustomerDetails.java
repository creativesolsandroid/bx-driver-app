package com.example.cs.bxdriver;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by CS on 12-08-2016.
 */
public class CustomerDetails extends AppCompatActivity {

    SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
    SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
    SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
    SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
    SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd-MM-yyyy hh:mm a",Locale.US);
    private String timeResponse = null;
    private CountDownTimer countDownTimer;

    private CountDownTimer countDownTimer1;
    TextView customerName, customerAddress, orderDeliveredTxt, expTime, travelTime, expTimer, travelTimer;
    LinearLayout navUser, call, message, orderDelivered;
    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    Order order;
    SharedPreferences userPrefs;
    String userId;
    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    private Timer timer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_details);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        order = (Order) getIntent().getSerializableExtra("order_obj");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navUser = (LinearLayout) findViewById(R.id.navigate_touser);
        customerName = (TextView) findViewById(R.id.customer_name);
        customerAddress = (TextView) findViewById(R.id.customer_address);
        call = (LinearLayout) findViewById(R.id.customer_call);
        message = (LinearLayout) findViewById(R.id.customer_message);
        orderDelivered = (LinearLayout) findViewById(R.id.order_delivered);
        orderDeliveredTxt = (TextView) findViewById(R.id.order_delivered_txt);
        expTime = (TextView) findViewById(R.id.expected_time);
        travelTime = (TextView) findViewById(R.id.travel_time);
        expTimer = (TextView) findViewById(R.id.exp_timer);
        travelTimer = (TextView) findViewById(R.id.travel_timer);

        customerName.setText(order.getFullName());
        customerAddress.setText(order.getAddress());
        Date expTime24 = null;
        try {
            expTime24 = timeFormat4.parse(order.getExpectedTime());
            Log.e("TAG", "response" + timeResponse);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String exptimeStr = null;
        exptimeStr = timeFormat4.format(expTime24);

        Log.e("TAG", "" + exptimeStr);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat time = new SimpleDateFormat("hh:mm a");
        expTime.setText("Exp Time: " + exptimeStr);
        travelTime.setText("--");

        navUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + order.getUserLat() + "," + order.getUserLong());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        orderDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdateOrderStatus().execute(Constants.live_url + "/api/DriverApp/GetUpdateOrderStatus?DriverId=" + userId + "&OrderId=" + order.getOrderId() + "&OrderStatus=Delivered&Comment=&TravelTime=");
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + order.getUserMobile()));
                        if (ActivityCompat.checkSelfPermission(CustomerDetails.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + order.getUserMobile()));
                    startActivity(intent);
                }
            }
        });

        new GetCurrentTime().execute();
//        timer.schedule(new MyTimerTask(), 10, 60000);
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new getTrafficTime().execute();
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    public class UpdateOrderStatus extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(CustomerDetails.this);
            dialog = ProgressDialog.show(CustomerDetails.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(CustomerDetails.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(CustomerDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);


                            orderDeliveredTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right, 0);
                            orderDelivered.setClickable(false);
                            Intent intent = new Intent(CustomerDetails.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(CustomerDetails.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(CustomerDetails.this);
            dialog = ProgressDialog.show(CustomerDetails.this, "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            } else {
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (serverTime == null) {
                dialog.dismiss();
            } else if (serverTime.equals("no internet")) {
                dialog.dismiss();
                Toast.makeText(CustomerDetails.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
                } catch (JSONException je) {
                    je.printStackTrace();
                }
                new getTrafficTime().execute();

            }


            super.onPostExecute(result1);
        }
    }

    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        String distanceResponse;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(CustomerDetails.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + MainActivity.lat + "," + MainActivity.longi + "&destinations=" + order.getUserLat() + "," + order.getUserLong() + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(CustomerDetails.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        String value = jo3.getString("value");
//                        if(language.equalsIgnoreCase("En")) {
                        travelTime.setText("Travel Time: " + secs);
//                        }else if(language.equalsIgnoreCase("Ar")){
//                            travelTimeText.setText(secs+ "  وقت السفر" );
//                        }
                        Date current24Date = null, currentServerDate = null;
                        Date expectedTimeDate = null, expectedTime24 = null;
                        try {
                            current24Date = timeFormat.parse(timeResponse);
                            expectedTime24 = timeFormat4.parse(order.getExpectedTime());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String currentTime = timeFormat1.format(current24Date);
                        String expTimeStr = timeFormat1.format(expectedTime24);
                        try {
                            currentServerDate = timeFormat1.parse(currentTime);
                            expectedTimeDate = timeFormat1.parse(expTimeStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long diff = expectedTimeDate.getTime() - currentServerDate.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        int expMins = (int) diffMinutes * 60 * 1000;
                        Log.i("TAG", "mins response: " + expMins);
                        int mins = (Integer.parseInt(value) / 60) + 1;
                        Calendar now = Calendar.getInstance();
                        now.setTime(currentServerDate);
                        now.add(Calendar.MINUTE, mins);
                        currentServerDate = now.getTime();
                        String CTimeString = timeFormat2.format(currentServerDate);
                        travelTime.setText("Travel Time: " + CTimeString);
                        int noOfMinutes = mins * 60 * 1000;//Convert minutes into milliseconds

                        startTimer(noOfMinutes);//start countdown
                        startTimer1(expMins);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            super.onPostExecute(result);

        }

    }


    //Start Countodwn method
    private void startTimer(int noOfMinutes) {
        countDownTimer = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                travelTimer.setText(hms);//set text
            }

            public void onFinish() {

                expTimer.setTextColor(Color.parseColor("#FF0000")); //On finish change timer text
//                countDownTimer = null;//set CountDownTimer to null
            }
        }.start();

    }

    //Start Countodwn method
    private void startTimer1(int noOfMinutes) {
        countDownTimer1 = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                expTimer.setText(hms);//set text
            }

            public void onFinish() {

//                expTimer.setText("TIME'S UP!!"); //On finish change timer text
//                countDownTimer1 = null;//set CountDownTimer to null
            }
        }.start();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(CustomerDetails.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + order.getUserMobile()));
                    if (ActivityCompat.checkSelfPermission(CustomerDetails.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                }
                else {
                    Toast.makeText(CustomerDetails.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
